import { Inject, Controller, Post, File, Fields } from '@midwayjs/core';
import { Context } from '@midwayjs/koa';
import { CreateSldDto } from '../dto/CreateSldDto';
import { ApiBody, BodyContentType } from '@midwayjs/swagger';
import { SldService } from '../service/sld.service';
@Controller('/sld')
export class APIController {
  @Inject()
  ctx: Context;

  @Inject()
  sldService: SldService;

  @Post('/', { summary: '规则文件上传' })
  @ApiBody({
    description: '规则文件',
    contentType: BodyContentType.Multipart,
    type: CreateSldDto,
  })
  async create(@File() file: any, @Fields() createSldDto: CreateSldDto) {
    if (!file) {
      this.ctx.status = 415;
      this.ctx.body = {
        status: 'error',
        message: '文件为必填项',
      };
    }
    return this.sldService.readClfFile(file,createSldDto)
    
  }
}
