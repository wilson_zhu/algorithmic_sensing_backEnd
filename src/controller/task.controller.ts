/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-26 16:09:04
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-07-17 10:26:26
 * @Description:
 */
import {
  Body,
  Controller,
  Del,
  File,
  Get,
  Inject,
  Param,
  Post,
  Put,
  Query,
} from '@midwayjs/core';
import { GetTaskDto,changeStausDto ,statusListenDto,createTaskDto,getTreeDto} from '../dto/TaskDto';
import { TaskService } from '../service/task.service';
@Controller('/task')
export class APIController {
  @Inject()
  taskService: TaskService;

  @Get('/', { summary: '任务列表' })
  async getList(@Query() params: GetTaskDto) {
    return await this.taskService.getTaskList(params);
  }

  @Del('/:id', { summary: '删除任务' })
  async deleteTask(@Param('id') id: number) {
    return await this.taskService.deleteTask(id);
  }

  @Get('/publish/:id', { summary: '发布成果' })
  async publishResult(@Param('id') id: number) {
    return await this.taskService.publishResult(id);
  }

  @Get('/tree', { summary: '树形任务列表' })
  async getTreeList(@Query() params: getTreeDto) {
    return await this.taskService.getTreeList(params.location);
  }
  @Put('/pushResult/:id', { summary: '结果推送' })
  async pushResult(@Param('id') id: number) {
    return await this.taskService.getFeature(id);
  }

  @Put('/changeStatus/:id', { summary: '状态修改' })
  async changeStatus(@Param('id') id: number,  @Body() params:changeStausDto ) {
    const { status } = params;
    return await this.taskService.changeStatus(id,status);
  }

  @Put('/statusListen', { summary: '状态监听自动处理' })
  async statusListen(  @Body() params:statusListenDto ) {
    const { status } = params;
    return await this.taskService.changeListenStatus(status);
  }

  @Put('/autoSeg', { summary: '自动分割启停' })
  async autoSeg(  @Body() params:statusListenDto ) {
    const { status } = params;
    return await this.taskService.autoSeg(status);
  }

  @Put('/autoPublish', { summary: '自动发布启停' })
  async autoPublish(  @Body() params:statusListenDto ) {
    const { status } = params;
    return await this.taskService.autoPublish(status);
  }

  @Post('/createTask',{ summary: '创建任务并执行' })
  async createTask(  @Body() params:createTaskDto ) {
    return await this.taskService.createTaskAndExecution(params);
  }

  @Post('/restartTask/:id',{ summary: '重启任务' })
  async restartTask( @Param('id') id: number,) {
    return await this.taskService.restartTask(id);
  }
}
