/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-22 16:57:20
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-02-19 17:16:34
 * @Description:
 */
import {
  Body,
  Controller,
  Del,
  File,
  Get,
  Inject,
  Param,
  Post,
  Put,
  Query,
} from '@midwayjs/core';
import { Context } from '@midwayjs/koa';
import { ApiBody, BodyContentType } from '@midwayjs/swagger';
import { AlgorithmService } from '../service/algorithm.service';
import { GetAlgorithmsDto, UpdateAlgorithmsDto } from '../dto/AlgorithmDto';
@Controller('/algorithm')
export class APIController {
  @Inject()
  ctx: Context;

  @Inject()
  algorithmService: AlgorithmService;

  @Post('/', { summary: '算法配置文件上传' })
  @ApiBody({
    description: '算法配置文件',
    contentType: BodyContentType.Multipart,
  })
  async create(@File() file: any) {
    return await this.algorithmService.createAlgorithm(file);
  }

  @Get('/', { summary: '算法列表' })
  async getList(@Query() params: GetAlgorithmsDto) {
    return await this.algorithmService.getAlgorithmList(params);
  }

  @Del('/:id', { summary: '删除算法' })
  async deleteAlgorithm(@Param('id') id: number) {
    return await this.algorithmService.deleteAlgorithm(id);
  }

  @Put('/:id', { summary: '更新算法' })
  @ApiBody({
    description: '更新算法的信息',
  })
  async updateAlgorithm(
    @Body() updateDto: UpdateAlgorithmsDto,
    @Param('id') id: number
  ) {
    return await this.algorithmService.updateAlgorithm(updateDto, id);
  }

  @Get('/:id', { summary: '执行算法' })
  async executeAlgorithm(@Param('id') id: number) {
    return await this.algorithmService.executeAlgorithm(id);
  }
}
