/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-23 14:42:59
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-01-23 14:44:59
 * @Description:
 */
import { ApiProperty } from '@midwayjs/swagger';

export class GetAlgorithmsDto {
  @ApiProperty({
    example: '1',
    description: '页码',
  })
  page_index: number;

  @ApiProperty({
    example: '1',
    description: '页码长度',
  })
  page_size:number ;

  @ApiProperty({
    description: '算法关键词',
  })
  keyword: string;
}

export class UpdateAlgorithmsDto {

  @ApiProperty({
    example: 'New Algorithm Name',
    description: '算法名称',
  })
  name?: string;

  @ApiProperty({
    example: 'new_identifier',
    description: '算法标识符',
  })
  identifier?: string;

  @ApiProperty({
    example: 'New algorithm description',
    description: '算法描述',
  })
  description?: string;

  @ApiProperty({
    example: '2.0',
    description: '算法版本',
  })
  version?: string;

  @ApiProperty({
    example: 'New Application',
    description: '应用程序',
  })
  application?: string;

  @ApiProperty({
    example: 'windows',
    description: '操作系统',
  })
  operating_system?: string;

  @ApiProperty({
    example: '{"param1": "value1", "param2": "value2"}',
    description: '参数',
  })
  parameters?: string;

}
