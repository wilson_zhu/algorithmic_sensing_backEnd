/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-12 16:30:50
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-01-15 16:28:14
 * @Description:
 */
import { ApiProperty } from '@midwayjs/swagger';

export class CreateSldDto {
  @ApiProperty({
    example: 'styleClf',
    description: '渲染文件名称',
  })
  name: string;

  @ApiProperty({
    example: '渲染文件描述',
    description: '渲染文件描述',
  })
  des: string;

  @ApiProperty({
    type: 'string',
    format: 'binary',
    description: '渲染文件实体文件',
  })
  file: any;
}
