/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-26 16:22:18
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-06-02 16:04:32
 * @Description:
 */
import { ApiProperty } from '@midwayjs/swagger';

export class GetTaskDto {
  @ApiProperty({
    example: '1',
    description: '页码',
  })
  page_index: number;

  @ApiProperty({
    example: '1',
    description: '页码长度',
  })
  page_size: number;

  @ApiProperty({
    description: '算法关键词',
  })
  keyword: string;
}
export class getTreeDto {
  @ApiProperty({
    example: '色林错',
    description: '色林错',
  })
  location: string;
}

export class changeStausDto {
  @ApiProperty({
    example: '5',
    description: '状态',
  })
  status: number;
}

export class statusListenDto {
  @ApiProperty({
    example: true,
    description: '状态',
  })
  status: boolean;
}

export class createTaskDto {
  @ApiProperty({
    example: '/GF1_WFV_030602/GF1_WFV_030602.tif',
    description: '输入文件(影像)路径',
  })
  inputFile: string;

  @ApiProperty({
    example: '/result_data/GF1_WFV_030602/GF1_WFV_030602.shp',
    description: '输出文件(矢量)路径',
  })
  outputFile: string;

  @ApiProperty({
    example: '2023/9/23',
    description: '输入文件日期',
  })
  fileDate: string;

  @ApiProperty({
    example: 'S2X_MSI',
    description: '卫星类型',
  })
  satType: string;

  @ApiProperty({
    example: '色林错',
    description: '湖库地区',
  })
  location: string;

  @ApiProperty({
    example: '[88.381872,31.482,89.3952,32.160239999999995]',
    description: '角点信息',
  })
  box: string;

  @ApiProperty({
    example: 14,
    description: '算法id',
  })
  algorithmId: number;

  @ApiProperty({
    example: '3d4963de24494d858aecafe8e8c6a8d4',
    description: '预处理id',
  })
  preId: string;

}
