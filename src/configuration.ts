/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-11 14:25:40
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-04-17 10:39:07
 * @Description:
 */
import { Configuration, App } from '@midwayjs/core';
import * as koa from '@midwayjs/koa';
import * as validate from '@midwayjs/validate';
import * as info from '@midwayjs/info';
import * as swagger from '@midwayjs/swagger';
import * as orm from '@midwayjs/typeorm';
import * as upload from '@midwayjs/upload';
import * as cron from '@midwayjs/cron';
import * as crossDomain from '@midwayjs/cross-domain';
import * as ws from '@midwayjs/ws';
import { join } from 'path';
import { ReportMiddleware } from './middleware/report.middleware';

@Configuration({
  imports: [
    koa,
    validate,
    upload,
    {
      component: info,
      enabledEnvironment: ['local'],
    },
    swagger,
    orm,
    cron,
    crossDomain,ws
  ],
  importConfigs: [join(__dirname, './config')],
})
export class MainConfiguration {
  @App('koa')
  app: koa.Application;
  async onReady() {
    this.app.useMiddleware([ReportMiddleware]);
  }
}
