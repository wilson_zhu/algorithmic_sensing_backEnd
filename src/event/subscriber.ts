/*
 * @Author: zhuxiaoyi
 * @Date: 2024-03-05 17:18:43
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-03-06 11:21:24
 * @Description:
 */
import { EventSubscriberModel } from '@midwayjs/typeorm';
import {
  EntitySubscriberInterface,
  InsertEvent,
  UpdateEvent,
  RemoveEvent,
} from 'typeorm';

@EventSubscriberModel()
export class EverythingSubscriber implements EntitySubscriberInterface {
  /*  beforeInsert(event: InsertEvent<any>) {
    console.log(`BEFORE ENTITY INSERTED: `, event.entity);
  } */
  /*   beforeUpdate(event: UpdateEvent<any>) {
    console.log(`BEFORE ENTITY UPDATED: `, event.entity);
  } */
  /*  beforeRemove(event: RemoveEvent<any>) {
    console.log(`BEFORE ENTITY WITH ID ${event.entityId} REMOVED: `, event.entity);
  } */
  /* afterInsert(event: InsertEvent<any>) {
    console.log(`AFTER ENTITY INSERTED: `, event.entity);
  } */
  /*   afterUpdate(event: UpdateEvent<any>) {
    console.log(`AFTER ENTITY UPDATED: `, event.entity);
  } */
  /* afterRemove(event: RemoveEvent<any>) {
    console.log(`AFTER ENTITY WITH ID ${event.entityId} REMOVED: `, event.entity);
  } */
  /*  afterLoad(entity: any) {
    console.log(`AFTER ENTITY LOADED: `, entity);
  } */
}
