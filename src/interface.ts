/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-11 14:25:40
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-06-02 11:26:44
 * @Description:
 */

/**
 * @description User-Service parameters
 */
export interface IUserOptions {
  uid: number;
}

export interface ISldOptions {
  name: string;

  des: string;

  file: any;
}

export interface Ipage {
  page_index: number;

  page_size: number;

  keyword: string;
}

export interface IAlgorithm {
  id?: number;

  name?: string;

  identifier?: string;

  description?: string;

  version?: string;

  application?: string;

  operating_system?: string;

  parameters?: string;

  creatsDate?: Date;

  updateDate?: Date;

  tasks?: any;
}

export interface Itask {
  id?: number;

  name?: string;

  status?: number;

  progress?: number;

  logFilePath?: string;

  finishTime?: Date;

  createDate?: Date;

  updateDate?: Date;

  Algorithm?: IAlgorithm;
}

export interface ItaskExecute {

  inputFile: string;

  outputFile: string;

  fileDate: string;

  satType: string;

  location: string;

  box: string;

  algorithmId:number;

  preId:string;
}
