/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-26 11:27:49
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-06-27 10:23:45
 * @Description:
 */
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToOne
} from 'typeorm';
import { Algorithm } from './algorithm.entity';
import { WaterFeature } from './feature.entity';

@Entity('tb_task_detial')
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: 100,
  })
  name: string;

  @Column()
  status: number;

  @Column()
  fileName: string;

  @Column({nullable: true})
  inputFile: string;

  @Column({nullable: true})
  randerFile: string;

  @Column({nullable: true})
  outputFile: string;

  @Column({nullable: true})
  shpZipFile: string;

  @Column({nullable: true})
  fileDate: Date;
  
  @Column({nullable: true})
  satType: string;

  @Column(
    {nullable: true}
  )
  progress: number;

  @Column({nullable: true})
  logId: number;

  @Column({nullable: true})
  location: string;

  @Column({nullable: true})
  gisInfo: string;

  @CreateDateColumn()
  createDate: Date;

  @UpdateDateColumn()
  updateDate: Date;

  @Column()
  isPublished: boolean;

  @Column({nullable: true})
  preId: string;

  // 设置与算法表的关联关系
  @ManyToOne(type => Algorithm, algorithm => algorithm.tasks)
  @JoinColumn()
  algorithm: Algorithm;


  @OneToOne(type => WaterFeature)
  @JoinColumn()
  waterFeature: WaterFeature;
}
