/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-12 14:45:10
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-01-16 18:34:01
 * @Description:
 */
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn,UpdateDateColumn } from 'typeorm';

@Entity('tb_l1pub_sld')
export class Sld {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: 100
  })
  name: string;

  @Column({
    length: 200
  })
  des: string;

  @Column('text')
  content: string;


  @Column()
  isPublished: boolean;

  @CreateDateColumn()
  creatsDate:Date;

  @UpdateDateColumn()
  updateDate:Date;


}
