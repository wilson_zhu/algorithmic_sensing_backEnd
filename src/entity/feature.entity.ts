/*
 * @Author: zhuxiaoyi
 * @Date: 2024-03-27 10:58:40
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-03-27 17:33:42
 * @Description:
 */
import { Entity, Column, PrimaryGeneratedColumn ,Geometry,OneToOne,JoinColumn} from 'typeorm';
import { Task } from './task.entity';
@Entity({ name: 'dss_water_features' })
export class WaterFeature {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ nullable: true })
  pre_id: string;

  @Column({  nullable: true })
  wrs_id: string;

  @Column({ type: 'geometry', nullable: true })
  geom: Geometry;

  @Column({ nullable: true })
  metadata_id: string;

  @Column({ type: 'timestamp', precision: 6, nullable: true })
  start_time: Date;

  @Column({ type: 'timestamp', precision: 6, nullable: true })
  end_time: Date;

  @Column({ type: 'timestamp', precision: 6, nullable: true })
  creat_time: Date;
}
