/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-12 14:45:10
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-04-17 11:40:00
 * @Description:
 */
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn,UpdateDateColumn,OneToMany   } from 'typeorm';
import { Task } from './task.entity';

@Entity('tb_l1pub_algorithm')
export class Algorithm {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: 100
  })
  name: string;

  @Column({
    length: 100
  })
  identifier: string;

  @Column()
  description: string;

  @Column()
  version: string;

  @Column()
  application: string;

  @Column()
  operating_system: string;

  @Column({  nullable: true })
  parameters: string;

  @CreateDateColumn()
  creatsDate:Date;

  @UpdateDateColumn()
  updateDate:Date;

  @OneToMany (type => Task, task => task.algorithm)
  tasks: Task[];

}
