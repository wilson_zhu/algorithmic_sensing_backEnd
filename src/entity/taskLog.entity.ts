/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-26 11:27:49
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-03-05 11:38:27
 * @Description:
 */
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('tb_task_log')
export class TaskLog {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  algorithmInfo: string;

  @CreateDateColumn()
  createDate: Date;

  @UpdateDateColumn()
  updateDate: Date;

}
