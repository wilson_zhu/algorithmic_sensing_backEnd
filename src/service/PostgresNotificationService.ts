import { Provide } from '@midwayjs/decorator';
const { Client } = require('pg');

@Provide()
export class PostgresNotificationService {
  constructor() {
    const client = new Client({
      user: 'citusadmin',
      host: '192.168.101.193',
      database: 'algorithm',
      password: 'citusadmin',
      port: '5432',
    });

    client.connect();
    client.query('LISTEN task_detail_status_change'); // 修改为触发器名称

    client.on('notification', async (msg) => {
      // console.log('Received notification:', msg.payload);
      // 在这里处理收到的通知，例如触发相应的逻辑
    });
  }
}
