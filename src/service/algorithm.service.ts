/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-22 17:04:45
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-03-06 18:07:04
 * @Description:
 */
import { Provide, Inject } from '@midwayjs/core';
import * as fs from 'fs';
import { InjectEntityModel } from '@midwayjs/typeorm';
import { IAlgorithm, Ipage } from '../interface';
import { Algorithm } from '../entity/algorithm.entity';
import { Repository, Like } from 'typeorm';
import { Context } from '@midwayjs/koa';
import { TaskService } from './task.service';

@Provide()
export class AlgorithmService {
  @InjectEntityModel(Algorithm)
  AlgorithmModel: Repository<Algorithm>;

  @Inject()
  ctx: Context;

  @Inject()
  taskService: TaskService;

  async createAlgorithm(file: any) {
    if (!file) {
      this.ctx.status = 415;
      this.ctx.body = {
        status: 'error',
        message: '文件为必填项',
      };
    }
    return await this.readJsonFileAndSave(file.data);
  }

  async readJsonFileAndSave(filePath: string) {
    try {
      // 读取文件内容
      const fileContent = fs.readFileSync(filePath, 'utf-8');

      // 尝试解析 JSON
      const jsonData = JSON.parse(fileContent);

      // 验证 JSON 结构
      if (this.validateJsonStructure(jsonData)) {
        // 将数据保存到数据库
        const algorithm = this.AlgorithmModel.create({
          name: jsonData.name,
          identifier: jsonData.identifier,
          description: jsonData.description,
          version: jsonData.version,
          application: jsonData.application,
          operating_system: jsonData.operating_system,
          parameters: JSON.stringify(jsonData.parameter), // 将参数数组转为字符串存储
        });

        await this.AlgorithmModel.save(algorithm);
        return { success: true, message: '数据保存成功' };
      } else {
        throw new Error('JSON 文件结构不符合预期');
      }
    } catch (error) {
      // 如果解析或保存失败，返回错误信息
      return {
        success: false,
        message: '处理 JSON 文件失败: ' + error.message,
      };
    }
  }

  async getAlgorithmList(params: Ipage) {
    const [result, total] = await this.AlgorithmModel.findAndCount({
      where: { name: Like(`%${params.keyword ? params.keyword : ''}%`) },
      skip: (params.page_index - 1) * params.page_size,
      take: params.page_size,
      order: { creatsDate: 'DESC' },
    });
    return {
      succes: true,
      data: result,
      total,
      message: '成功',
    };
  }

  async deleteAlgorithm(id: number) {
    const algorithm = await this.AlgorithmModel.findOne({
      where: {
        id,
      },
    });
    if (!algorithm) {
      return {
        succes: false,
        message: '未查询到要删除的数据',
      };
    }

    // 删除单个
    let result = await this.AlgorithmModel.remove(algorithm);
    return {
      succes: true,
      data: result.identifier,
      message: '成功',
    };
  }

  async updateAlgorithm(updateDto: IAlgorithm, id: number) {
    const algorithm = await this.AlgorithmModel.findOne({
      where: { id },
    });

    if (!algorithm) {
      return {
        success: false,
        message: '未找到要更新的算法',
      };
    }
    // 更新算法信息
    Object.assign(algorithm, updateDto);
    const updatedAlgorithm = await this.AlgorithmModel.save(algorithm);

    return {
      success: true,
      data: updatedAlgorithm,
      message: '算法更新成功',
    };
  }

  async executeAlgorithm(id: number) {

    const algorithm = await this.AlgorithmModel.findOne({
      where: { id },
    });

    if (!algorithm) {
      return {
        success: false,
        message: '未找到要执行的算法',
      };
    }

    const task = await this.taskService.createTask(algorithm);
    if (!task) {
      return {
        success: false,
        message: '任务创建失败',
      };
    }

    return {
      success: true,
      data: {
        taskId: task.id,
      },
      message: '任务创建成功',
    };
  }

  private validateJsonStructure(jsonData: any): boolean {
    // 验证 JSON 结构是否符合预期
    return (
      jsonData &&
      typeof jsonData === 'object' &&
      'name' in jsonData &&
      'identifier' in jsonData &&
      'description' in jsonData &&
      'version' in jsonData &&
      'application' in jsonData &&
      'operating_system' in jsonData &&
      'parameter' in jsonData &&
      Array.isArray(jsonData.parameter)
    );
  }


}
