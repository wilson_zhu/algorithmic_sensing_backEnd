/*
 * @Author: zhuxiaoyi
 * @Date: 2024-03-06 11:59:07
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-04-08 12:58:55
 * @Description:
 */
import {
  Body,
  Controller,
  Del,
  File,
  Get,
  Inject,
  Param,
  Post,
  Put,
  Query,
} from '@midwayjs/core';
import { Job, IJob } from '@midwayjs/cron';
import { FORMAT } from '@midwayjs/core';
import { TaskService } from '../service/task.service';

@Job({
  cronTime: FORMAT.CRONTAB.EVERY_PER_10_SECOND,
  // start: true,
  start: false,
})
export class DataSyncCheckerJob implements IJob {

  @Inject()
  taskService:TaskService ;
  async onTick() {
    this.taskService.executeTaskByStatus()
  }
}

@Job({
  cronTime: FORMAT.CRONTAB.EVERY_PER_10_SECOND,
  // start: true,
  start: false,
})
export class listenTasktoPublish implements IJob {

  @Inject()
  taskService:TaskService ;
  async onTick() {
    this.taskService.publishTaskByStatus()
  }
}
