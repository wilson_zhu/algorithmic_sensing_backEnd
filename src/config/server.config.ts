/*
 * @Author: zhuxiaoyi
 * @Date: 2024-04-17 11:12:23
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-06-02 16:18:50
 * @Description:
 */
import { Sld } from '../entity/sld.entity';
import { Algorithm } from '../entity/algorithm.entity';
import { Task } from '../entity/task.entity';
import { TaskLog } from '../entity/taskLog.entity';
import { WaterFeature } from '../entity/feature.entity';
import { EverythingSubscriber } from '../event/subscriber';

export default {
  algorithmBaseURL: "D:/zyjx",
  dataBaseURL: "D:/SLC_DATA",
  geoServerURL: 'http://192.168.107.224:8055/geoserver',
  tmpdir:"C:/slc_sys/templateFiles",
  dataSource: {
    default: {
      type: 'postgres',
      host: '192.168.107.221',
      port: 5432,
      username: 'citusadmin',
      password: 'citusadmin',
      database: 'algorithm',
      synchronize: false, // 如果第一次使用，不存在表，有同步的需求可以写 true，注意会丢数据
      logging: false,
      // 配置实体模型
      entities: [Sld, Algorithm, Task, TaskLog, WaterFeature],
      subscribers: [EverythingSubscriber],
      extra: {
        postgis: true,
      },
    },
  },
}
