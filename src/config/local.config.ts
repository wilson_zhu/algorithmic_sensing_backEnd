/*
 * @Author: 
 * @Date: 2024-05-31 15:58:42
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-06-02 11:42:12
 * @Description: 
 */
import { Sld } from '../entity/sld.entity';
import { Algorithm } from '../entity/algorithm.entity';
import { Task } from '../entity/task.entity';
import { TaskLog } from '../entity/taskLog.entity';
import { WaterFeature } from '../entity/feature.entity';
import { EverythingSubscriber } from '../event/subscriber';

export default {
  algorithmBaseURL: "D:/Temp",
  dataBaseURL: "D:/Temp",
  geoServerURL: 'http://localhost:8055/geoserver',
  tmpdir: 'C:/templateFiles',
  dataSource: {
    default: {
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'zyjy2021',
      database: 'algorithm',
      synchronize: true, // 如果第一次使用，不存在表，有同步的需求可以写 true，注意会丢数据
      logging: false,
      // 配置实体模型
      entities: [Sld, Algorithm, Task, TaskLog, WaterFeature],
      subscribers: [EverythingSubscriber],
      extra: {
        postgis: true,
      },
    },
  },
  
};
