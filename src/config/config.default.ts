/*
 * @Author: zhuxiaoyi
 * @Date: 2024-01-11 14:25:40
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-06-02 10:12:17
 * @Description:
 */
import { MidwayConfig } from '@midwayjs/core';
import { join, dirname } from 'path';
import localConfig from './local.config';
import cangqiongConfig from './server.config';

let currentENV = 'LOCAL';
// let currentENV = "SERVER"

let currentConfig;
switch (currentENV) {
  case 'LOCAL':
    currentConfig = localConfig;
    break;
  case 'SERVER':
    currentConfig = cangqiongConfig;
    break;
  default:
    break;
}

export default {
  algorithmBaseURL: currentConfig.algorithmBaseURL,
  dataBaseURL: currentConfig.dataBaseURL,
  geoServerURL: currentConfig.geoServerURL,
  keys: '1704764784547_8760',
  koa: {
    port: 7001,
    globalPrefix: '/algorithmApi',
  },
  typeorm: {
    dataSource: currentConfig.dataSource,
  },
  upload: {
    // mode: UploadMode, 默认为file，即上传到服务器临时目录，可以配 置为 stream
    mode: 'file',
    // fileSize: string, 最大上传文件大小，默认为 10mb
    fileSize: '500mb',
    // whitelist: string[]，文件扩展名白名单
    whitelist: ['.clf', '.json'],
    // tmpdir: string，上传的文件临时存储路径
    // tmpdir: join(tmpdir(), 'midway-upload-files'),
    tmpdir: currentConfig.tmpdir,
    // cleanTimeout: number，上传的文件在临时目录中多久之后自动删除，默认为 5 分钟
    cleanTimeout: 5 * 60 * 1000,
    // base64: boolean，设置原始body是否是base64格式，默认为false，一般用于腾讯云的兼容
    base64: false,
    // 仅在匹配路径到 /api/upload 的时候去解析 body 中的文件信息
    match: [/\/algorithmApi\/sld/, /\/algorithmApi\/algorithm/],
  },
  cors: {
    origin: '*',
  },
  webSocket: {
    port: 8001,
  },
} as MidwayConfig;
