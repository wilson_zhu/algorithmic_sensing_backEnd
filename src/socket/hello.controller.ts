/*
 * @Author: zhuxiaoyi
 * @Date: 2024-04-02 18:07:50
 * @LastEditor: zhuxiaoyi
 * @LastEditTime: 2024-04-07 16:39:59
 * @Description:
 */
import { WSController, OnWSConnection, OnWSMessage,Inject,WSBroadCast ,App } from '@midwayjs/core';
import { Context  } from '@midwayjs/ws';
import * as http from 'http';

@WSController("/webSocket")
export class HelloSocketController {

  @Inject()
  ctx: Context;


  @OnWSConnection()
  async onConnectionMethod(socket: Context, request: http.IncomingMessage) {
    console.log(`namespace /webSocket got a connection ${this.ctx.readyState}`);
  }

  @OnWSMessage('message')
  @WSBroadCast()
  async gotMyMessage(data) {
    return { name: 'harry', result: parseInt(data) + 5 };
  }
}
